<?php

/**
 * @file
 * API documentation file for the entity cache Warmer module.
 */

/**
 * Perform alterations to the query which selects a next batch of entities to
 * be warmed up.
 *
 * See the documentation for Query Alteration and hook_query_TAG_alter() for
 * more information.
 *
 * The following metadata is available: entity_type and limit.
 *
 * @see https://drupal.org/node/310077
 */
function hook_query_entitycache_warmer_alter(QueryAlterableInterface $query) {
  $entity_type = $query->getMetaData('entity_type');
  if ($entity_type === 'node') {
    // Prioritize nodes based on their last changed date
    $query->orderBy('bt.changed', 'DESC');
  }
}

/**
 * Perform alterations to the query which selects a next batch of entities (of a
 * specific entity type) to be warmed up.
 *
 * See the documentation for Query Alteration and hook_query_TAG_alter() for
 * more information.
 *
 * The following metadata is available: entity_type and limit.
 *
 * @see https://drupal.org/node/310077
 */
function hook_query_entitycache_warmer_ENTITY_TYPE_alter(QueryAlterableInterface $query) {
  // Order the entities by random.
  $query->orderRandom();
}

/**
 * Perform alterations to entities right before they are added to the tracking
 * table.
 *
 * @param $items array
 *   Associative array, keyed by entity type and as value an array containing
 *   all applicable entity id's.
 */
function hook_entitycache_warmer_set_tracker_alter(&$items) {
  // Don't track the entity for the admin user.
  if (isset($items['user'])) {
    $key = array_search(1, $items['user']);
    if ($key !== FALSE) {
      unset($items[$key]);
    }
  }
}

/**
 * Perform alterations to entities right before they are cleared from the
 * tracking table.
 *
 * If $items is set to TRUE, all entities will be cleared from the tracking
 * table.
 *
 * @param $items array
 *   Associative array, keyed by entity type and as value an array containing
 *   all applicable entity id's.
 */
function hook_entitycache_warmer_reset_tracker_alter(&$items) {
  // Always remove the entity for the admin user.
  if (isset($items['user'])) {
    $key = array_search(1, $items['user']);
    if ($key !== FALSE) {
      unset($items[$key]);
    }
  }
}
