<?php

/**
 * @file
 * Drush commands for the Entity cache Warmer module.
 */

/**
 * Implementation of hook_drush_help().
 */
function entitycache_warmer_drush_help($section) {
  switch ($section) {
    case 'drush:entitycache-warmer-execute':
      return dt("First (and only) parameter is a list of entity types to be warmed up. Use 'all' to warm up all configured entity types. Used without parameter, the user will be prompted to select either all or one of the configured entity types.");
  }
}

/**
 * Implements hook_drush_command().
 */
function entitycache_warmer_drush_command() {
  $items = array();

  $items['entitycache-warmer-execute'] = array(
    'description' => 'Warm up entities in the entitycache.',
    'arguments' => array(
      'entity_types' => dt("Warm up one or more particular entity types, comma seperated. If 'all', all entity types will be assumed."),
    ),
    'options' => array(
      'limit' => dt("Set a limit on the number of entities that will be warmed. Defaults to !default_limit. Use 'all' to warm up all entities into the entity cache.", array('!default_limit' => ENTITYCACHE_WARMER_DEFAULT_CRON_LIMIT)),
      'batch-limit' => dt("If limit is 'all', warming of entities will be done in a batch. Configure a limit per batch process. Defaults to !default_limit.", array('!default_limit' => ENTITYCACHE_WARMER_DEFAULT_CRON_LIMIT)),
    ),
    'callback' => 'drush_entitycache_warmer_execute',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('ew-execute', 'ewe'),
  );

  return $items;
}

/**
 * Drush callback for the 'entitycache-warmer-execute' command.
 *
 * @param array $entity_types
 *   One or more entity types.
 */
function drush_entitycache_warmer_execute($entity_types) {
  // Validate the given entity type(s).
  $entity_types = _drush_entitycache_warmer_validate_entity_types($entity_types);
  if ($entity_types === FALSE) {
    return FALSE;
  }

  // Get the limit, if given.
  $limit = drush_get_option('limit', ENTITYCACHE_WARMER_DEFAULT_CRON_LIMIT);

  // Entity cache warming is run through a batch if all (remaining) entities
  // should be warmed up.
  if ($limit === 'all') {
    $config = array(
      'operations' => array(),
      'finished' => '_drush_entitycache_warmer_batch_finished',
      'title' => dt('Warming the entity cache.'),
      'init_message' => dt('Preparing the entity cache warmer.'),
      'progress_message' => dt('Warming entities into the entity cache...'),
      'error_message' => dt('An error occured warming the entity cache.'),
    );

    // A limit per batch process can be configured.
    $batch_process_limit = drush_get_option('batch-limit', ENTITYCACHE_WARMER_DEFAULT_CRON_LIMIT);
    foreach ($entity_types as $entity_type) {
      $config['operations'][] = array(
        '_drush_entitycache_warmer_batch_process', array($entity_type, $batch_process_limit),
      );
    }
    batch_set($config);

    $batch =& batch_get();
    $batch['progressive'] = FALSE;
    drush_backend_batch_process();
  }
  else {
    foreach ($entity_types as $entity_type) {
      entitycache_warmer_execute($entity_type, $limit);
    }
  }
}

/**
 * Batch worker callback for warming up the the entity cache.
 *
 * @param string $entity_type
 *   Type of entities.
 * @param int $batch_process_limit
 *   How many entities to process.
 * @param array $context
 *   Batch API contextual information.
 */
function _drush_entitycache_warmer_batch_process($entity_type, $batch_process_limit, &$context) {
  // Warm up entities.
  $entity_ids = entitycache_warmer_execute($entity_type, $batch_process_limit);
  // Add the collected (cached) entities to the tracking table. Calling this
  // manually since hook_drush_exit() is not called after each batch process.
  entitycache_warmer_set_tracker();

  // If no entities we're returned, it means we are finished.
  if (empty($entity_ids)) {
    $context['finished'] = 1;
  }
  else {
    $context['finished'] = 0;
    $entity_info = entity_get_info($entity_type);
    $context['message'] = t("Processing entities of type !entity_type...", array('!entity_type' => $entity_info['label']));
  }
}

/**
 * Batch finish callback for warming up the entity cache.
 */
function _drush_entitycache_warmer_batch_finished() {
  drush_log(dt('Finished warming up the entity cache.'));
}

/**
 * Validates the given entity type(s) for Entity cache warmer drush commands.
 *
 * @param mixed $entity_types
 *   (optional) One (string) or multiple (array) entity types. If not given,
 *   an interactive drush choice menu is presented. Value 'all' is used for
 *   all valid entity types.
 */
function _drush_entitycache_warmer_validate_entity_types($entity_types = NULL) {
  $supported_entity_types = entitycache_supported_core_entities(TRUE);
  $entity_types_list = array_combine(array_keys($supported_entity_types), array_keys($supported_entity_types));

  if ($entity_types) {
    if ($entity_types !== 'all') {
      if (is_string($entity_types)) {
        $entity_types = explode(',', $entity_types);
      }
    }
  }
  else {
    $entity_types = drush_choice(array('all' => 'all') + $entity_types_list, 'Enter a number to choose which entity type you want this command to operate for.', '!key');
    if (!$entity_types) {
      return drush_set_error('Stopped because no valid entity types were was given.');
    }
  }

  if ($entity_types === 'all') {
    $entity_types = array_keys($entity_types_list);
  }
  elseif (!is_array($entity_types)) {
    $entity_types = array($entity_types);
  }

  return $entity_types;
}
