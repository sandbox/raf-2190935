<?php

/**
 * @file
 * Administrative page callbacks for the Entity cache Warmer module.
 */

/**
 * Form builder; Configure Entity cache Warmer settings.
 */
function entitycache_warmer_settings() {
  $form = array();

  // Entity types.
  $form['entitycache_warmer_entity_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Entity types'),
    '#description' => t('Select each entity type you wish to pre-emptively warm up the entity cache for. Only entity types compatible with entity cache can be selected.'),
    '#options' => array(),
    '#default_value' => variable_get('entitycache_warmer_entity_types', array()),
  );

  $entity_info = entity_get_info();
  foreach ($entity_info as $entity_type => $info) {
    $form['entitycache_warmer_entity_types']['#options'][$entity_type] = $info['label'];
    // Disable entity types incompatible with entity cache.
    if (!isset($info['entity cache']) || $info['entity cache'] !== TRUE || !isset($info['base table']) || empty($info['base table']) || !db_table_exists($info['base table'])) {
      $form['entitycache_warmer_entity_types'][$entity_type] = array(
        '#disabled' => TRUE,
      );
    }
  }

  // Cron limit.
  $form['entitycache_warmer_cron_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Cron limit'),
    '#description' => t('Entities are added to their entity cache by simply loading them using !entity_load. Configure how many entities may be loaded at once. The default is !cron_limit. Depending on the server capacity and complexity of your entities, this number may be increased. If 0 (zero) is entered, entity cache warming through cron will be disabled, and the drush command (ew-execute) will have to be used.', array('!cron_limit' => ENTITYCACHE_WARMER_DEFAULT_CRON_LIMIT, '!entity_load' => l('entity_load()', 'https://api.drupal.org/api/drupal/includes%21common.inc/function/entity_load/7'))),
    '#default_value' => variable_get('entitycache_warmer_cron_limit', ENTITYCACHE_WARMER_DEFAULT_CRON_LIMIT),
  );

  // Debug.
  $form['entitycache_warmer_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug'),
    '#description' => t("Various processes within Drupal may automatically induce a full entity cache clear. In order to track down these processes, debugging can be enabled. A compact !debug_backtrace, together with a date and current user, will be written to a file called entitycache_warmer_debug.log in the site's temporary folder.", array('!debug_backtrace' => l('debug_backtrace()', 'http://be2.php.net/debug_backtrace'))),
    '#default_value' => variable_get('entitycache_warmer_debug', 0),
  );

  return system_settings_form($form);
}

/**
 * Validation callback for the entity cache warmer settings form.
 */
function entitycache_warmer_settings_validate($form, &$form_state) {
  $form_state['values']['entitycache_warmer_entity_types'] = array_filter($form_state['values']['entitycache_warmer_entity_types']);
}

/**
 * Form builder; Manually warm up the entity cache.
 */
function entitycache_warmer_warm() {
  $form = array();

  // Entity types.
  $form['entitycache_warmer_entity_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Entity types'),
    '#description' => t('Select the entity types you manually want to warm up.'),
    '#options' => array(),
    '#default_value' => variable_get('entitycache_warmer_entity_types', array()),
  );

  $entity_info = entity_get_info();
  $entity_types = variable_get('entitycache_warmer_entity_types', array());
  foreach ($entity_types as $entity_type) {
    if (isset($entity_info[$entity_type])) {
      $form['entitycache_warmer_entity_types']['#options'][$entity_type] = $entity_info[$entity_type]['label'];
    }
  }

  // Batch process limit.
  $form['entitycache_warmer_batch_process_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Batch process limit'),
    '#description' => t('Enter the batch process limit. Entities are added to their entity cache by simply loading them using !entity_load. Configure how many entities may be loaded at once (per batch process).', array('!entity_load' => l('entity_load()', 'https://api.drupal.org/api/drupal/includes%21common.inc/function/entity_load/7'))),
    '#default_value' => variable_get('entitycache_warmer_cron_limit', ENTITYCACHE_WARMER_DEFAULT_CRON_LIMIT),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Start'));

  return $form;
}

/**
 * Submit handler for entitycache_warmer_warm.
 */
function entitycache_warmer_warm_submit($form, &$form_state) {
  $entity_types = $form_state['values']['entitycache_warmer_entity_types'];
  $batch_process_limit = $form_state['values']['entitycache_warmer_batch_process_limit'];

  $batch = array(
    'title' => t('Warming up the entity cache'),
    'operations' => array(),
    'finished' => 'entitycache_warmer_warm_batch_finish',
    'file' => drupal_get_path('module', 'entitycache_warmer') . '/entitycache_warmer.admin.inc',
  );

  foreach ($entity_types as $entity_type) {
    $batch['operations'][] = array('entitycache_warmer_warm_worker', array($entity_type, $batch_process_limit));
  }

  batch_set($batch);
}

/**
 * Batch worker callback for warming up the the entity cache.
 *
 * @param string $entity_type
 *   Type of entities.
 * @param int $batch_process_limit
 *   How many entities to process.
 * @param array $context
 *   Batch API contextual information.
 */
function entitycache_warmer_warm_worker($entity_type, $batch_process_limit, &$context) {
  // Initialize the sandbox on first run.
  if (empty($context['sandbox'])) {
    $context['sandbox']['count'] = 0;
  }

  // Warm up entities.
  $entity_ids = entitycache_warmer_execute($entity_type, $batch_process_limit);
  // Add the collected (cached) entities to the tracking table. Calling this
  // manually since hook_exit() is not called after each batch process.
  entitycache_warmer_set_tracker();

  // If no entities we're returned, it means we are finished.
  if (empty($entity_ids)) {
    $context['finished'] = 1;
  }
  else {
    $context['sandbox']['count'] += count($entity_ids);
    $context['finished'] = 0;
    $entity_info = entity_get_info($entity_type);
    $context['message'] = t("Processed !count entities of type !entity_type...", array('!count' => $context['sandbox']['count'], '!entity_type' => $entity_info['label']));
  }
}

/**
 * Batch finish callback for warming up the entity cache.
 */
function entitycache_warmer_warm_batch_finish() {
  drupal_set_message(t('Finished warming up the entity cache for the selected entity types.'), 'status');
}

/**
 * Form builder; Manually warm up the entity cache.
 */
function entitycache_warmer_clear() {
  $form = array();

  // Entity types.
  $form['entitycache_warmer_entity_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Entity types'),
    '#description' => t('Select the entity types you wish to clear the entity cache for.'),
    '#options' => array(),
    '#default_value' => variable_get('entitycache_warmer_entity_types', array()),
  );

  $entity_info = entity_get_info();
  $entity_types = variable_get('entitycache_warmer_entity_types', array());
  foreach ($entity_types as $entity_type) {
    if (isset($entity_info[$entity_type])) {
      $form['entitycache_warmer_entity_types']['#options'][$entity_type] = $entity_info[$entity_type]['label'];
    }
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Start'));

  return $form;
}

/**
 * Submit handler for entitycache_warmer_warm.
 */
function entitycache_warmer_clear_submit($form, &$form_state) {
  $entity_types = $form_state['values']['entitycache_warmer_entity_types'];

  foreach ($entity_types as $entity_type) {
    entity_get_controller($entity_type)->resetCache();
  }

  drupal_set_message(t('The entity cache was cleared for the selected entity types.'), 'status');
}
